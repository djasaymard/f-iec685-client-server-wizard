# C++/Qt F-IEC685 client server wizard #

Contributors: Aymard Djasrabé

Small program that allows exchanges between client and server.
We have arbitrarily defined a communication protocol that allows the client to send command or search packets to the server.

A MySQL database contains the data of the devices that are under the control of the server.

### Protocole ###

F-IEC685 structure (F for Fake)
  (See src/lib/logic/protocole/messageunit.h)
    
![Protocole Datagram](doc/img/F-IEC685-Datagram.png)



### Dependencies : ###

* C++ 14 compiler
* Boost.Test
* qMake (Qt)
* Doxygen(Optionnal). See the Doxyfile in doc/Doxygen to generate the documentation 
      on local machine


### To do ###

* finalize the unit tests


### License ###

* Free Software licence
