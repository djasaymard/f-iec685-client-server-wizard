#define BOOST_TEST_MODULE  TestEnergy
#include <boost/test/unit_test.hpp>


#include "../src/lib/logic/client/clientlogicengine.h"

ClientLogicEngine * client = new ClientLogicEngine();

BOOST_AUTO_TEST_SUITE(TestEnergy);


BOOST_AUTO_TEST_CASE(testcase1){

    BOOST_CHECK_EQUAL(10, client->sum(4, 6));  //for test of environnement
}


BOOST_AUTO_TEST_CASE(testcase2){

  BOOST_CHECK_EQUAL(18, client->sum(7, 11));
}

BOOST_AUTO_TEST_SUITE_END();
