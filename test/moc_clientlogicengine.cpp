/****************************************************************************
** Meta object code from reading C++ file 'clientlogicengine.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/lib/logic/client/clientlogicengine.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'clientlogicengine.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_ClientLogicEngine_t {
    QByteArrayData data[19];
    char stringdata0[248];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_ClientLogicEngine_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_ClientLogicEngine_t qt_meta_stringdata_ClientLogicEngine = {
    {
QT_MOC_LITERAL(0, 0, 17), // "ClientLogicEngine"
QT_MOC_LITERAL(1, 18, 18), // "dataAvailableForUi"
QT_MOC_LITERAL(2, 37, 0), // ""
QT_MOC_LITERAL(3, 38, 15), // "connectToServer"
QT_MOC_LITERAL(4, 54, 11), // "sendRequest"
QT_MOC_LITERAL(5, 66, 8), // "qVariant"
QT_MOC_LITERAL(6, 75, 11), // "onConnected"
QT_MOC_LITERAL(7, 87, 11), // "onReadyRead"
QT_MOC_LITERAL(8, 99, 10), // "stopSearch"
QT_MOC_LITERAL(9, 110, 24), // "connectionClosedByServer"
QT_MOC_LITERAL(10, 135, 5), // "error"
QT_MOC_LITERAL(11, 141, 24), // "handleDataAvailableForUi"
QT_MOC_LITERAL(12, 166, 15), // "sendRequestInfo"
QT_MOC_LITERAL(13, 182, 8), // "ipSource"
QT_MOC_LITERAL(14, 191, 13), // "ipDestination"
QT_MOC_LITERAL(15, 205, 11), // "idEquipment"
QT_MOC_LITERAL(16, 217, 2), // "ip"
QT_MOC_LITERAL(17, 220, 8), // "areaName"
QT_MOC_LITERAL(18, 229, 18) // "sendRequestCommand"

    },
    "ClientLogicEngine\0dataAvailableForUi\0"
    "\0connectToServer\0sendRequest\0qVariant\0"
    "onConnected\0onReadyRead\0stopSearch\0"
    "connectionClosedByServer\0error\0"
    "handleDataAvailableForUi\0sendRequestInfo\0"
    "ipSource\0ipDestination\0idEquipment\0"
    "ip\0areaName\0sendRequestCommand"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_ClientLogicEngine[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   70,    2, 0x0a /* Public */,
       4,    1,   71,    2, 0x0a /* Public */,
       6,    0,   74,    2, 0x0a /* Public */,
       7,    0,   75,    2, 0x0a /* Public */,
       8,    0,   76,    2, 0x0a /* Public */,
       9,    0,   77,    2, 0x0a /* Public */,
      10,    0,   78,    2, 0x0a /* Public */,
      11,    0,   79,    2, 0x0a /* Public */,
      12,    5,   80,    2, 0x0a /* Public */,
      18,    2,   91,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::QStringList,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QVariant,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::QStringList,
    QMetaType::Void, QMetaType::QString, QMetaType::QString, QMetaType::UChar, QMetaType::QString, QMetaType::QString,   13,   14,   15,   16,   17,
    QMetaType::Void, QMetaType::QString, QMetaType::QString,   13,   14,

       0        // eod
};

void ClientLogicEngine::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        ClientLogicEngine *_t = static_cast<ClientLogicEngine *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { QStringList _r = _t->dataAvailableForUi();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = std::move(_r); }  break;
        case 1: _t->connectToServer(); break;
        case 2: _t->sendRequest((*reinterpret_cast< QVariant(*)>(_a[1]))); break;
        case 3: _t->onConnected(); break;
        case 4: _t->onReadyRead(); break;
        case 5: _t->stopSearch(); break;
        case 6: _t->connectionClosedByServer(); break;
        case 7: _t->error(); break;
        case 8: { QStringList _r = _t->handleDataAvailableForUi();
            if (_a[0]) *reinterpret_cast< QStringList*>(_a[0]) = std::move(_r); }  break;
        case 9: _t->sendRequestInfo((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2])),(*reinterpret_cast< const quint8(*)>(_a[3])),(*reinterpret_cast< const QString(*)>(_a[4])),(*reinterpret_cast< const QString(*)>(_a[5]))); break;
        case 10: _t->sendRequestCommand((*reinterpret_cast< const QString(*)>(_a[1])),(*reinterpret_cast< const QString(*)>(_a[2]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = QStringList (ClientLogicEngine::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&ClientLogicEngine::dataAvailableForUi)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject ClientLogicEngine::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ClientLogicEngine.data,
      qt_meta_data_ClientLogicEngine,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *ClientLogicEngine::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *ClientLogicEngine::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_ClientLogicEngine.stringdata0))
        return static_cast<void*>(this);
    return QObject::qt_metacast(_clname);
}

int ClientLogicEngine::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
QStringList ClientLogicEngine::dataAvailableForUi()
{
    QStringList _t0{};
    void *_a[] = { const_cast<void*>(reinterpret_cast<const void*>(&_t0)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
    return _t0;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
