/**
* @file messageunit.h
* @brief Defines F_iec685 protocole format
* @see pduconstvalues.h
* @date 10/2020
* @author Aymard Djasrabé M.
*/

#ifndef MESSAGEUNIT_H
#define MESSAGEUNIT_H

#include <QDate>
#include <QMetaType>
#include <QTime>
#include <type_traits>

/*

         F_iec685 Datagram
         ----------------
  (See pduconstvalues.h for values)

    ---------------------
    |  Type of Message  |
    ---------------------------
    |     IP Source     |   |
    ---------------------   | Address
    |   IP Destination  |   |
    ---------------------------
    |    ID Equipment   |   |
    ---------------------   |
    |  Equipment Name   |   |
    ---------------------   |
    |        IP         |   | IDU
    ---------------------   |
    |      Area Name    |   |
    ---------------------   |
    |       Type        |   |
    ---------------------------
    | Command Identifier|
    ---------------------
*/


/**
 * @struct MessageUnit<false>
 * @brief Struct added to F_iec685 PDU if bool = false
 */
template <bool>
struct MessageUnit{
   quint16 commandIdentifier; //!< specify which command to execute
};

/**
* @struct MessageUnit<true>
* @brief Struct added to F_iec685 PDU if bool = true
*/
template <>
struct MessageUnit<true>
{
    /**
    * @struct IDU
    * @brief Information Data Unit in F_iec685PDU<true> Datagram
    */
   struct IDU{
      quint8 idEquipment;
      QString equipmentName;
      QString ip;
      QString areaName;
      QString type;
   };
   IDU idu;
};

/**
* @struct F_iec685PDU
* @brief F_iec685PDU Base Datagram
*/
template <bool B>
struct F_iec685PDU : MessageUnit<B>
{
   quint8 typeOfMessage; //!< Info or Command message
   /**
   * @struct Address
   * @brief Address of machine source and machine destination
   */
   struct Address{
      QString ipSource;
      QString ipDestination;
   };
   Address addr;
};
Q_DECLARE_METATYPE(F_iec685PDU<true>*)
Q_DECLARE_METATYPE(F_iec685PDU<false>*)

#endif // MESSAGEUNIT_H



