/**
*@file pduconstvalues.h
*@brief Define constant values for F_iec685 PDU structure
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#ifndef PDUCONSTVALUES_H
#define PDUCONSTVALUES_H

   #define REQUEST_INFO   1
   #define REPLY_INFO     3
   #define REQUEST_CMD    4
   #define REPLY_CMD      4

   #define COMMAND_STATUT_OK_REPLY   4
   #define COMMAND_STATUT_FAIL_REPLY 5

   #define COMMAND_OPEN_CMD     1
   #define COMMAND_OPEN_MIC     2
   #define COMMAND_OTHER_ACTION 3

#endif // PDUCONSTVALUES_H
