/**
*@file clientlogicengine.cpp
*@brief implemenation of class : ClientLogicEngine
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#include "clientlogicengine.h"

/**
 * @brief Constructor
 * @details init the messageField, connects Signal ans slots
*/
ClientLogicEngine::ClientLogicEngine()
{
   messageField.setValue(nullptr);
   connect(&socket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
   connect(this, SIGNAL(dataAvailableForUi()), this, SLOT(handleDataAvailableForUi()));
}

/**
 * @brief Connection to the server
 * @todo got ip adress as parameter
*/
void ClientLogicEngine::connectToServer()
{
   socket.connectToHost(QHostAddress("192.168.1.2"), 4242);
   qDebug()<<"Connecting to server";
   nextBlockSize = 0;
}

ClientLogicEngine::~ClientLogicEngine(){

}

/**
 * @brief Initialyze data of tye Info to send to server
 * @details The messageField parameter helps to hold both Information message and Command message
 * @param[in] messageField,
 * @param[in] infoPacket
 */  
void ClientLogicEngine::initializeInfoMessageUnit(QVariant messageField, const F_iec685PDU<true> infoPacket)
{
   nextBlockSize = 0;

 //  if(messageField.canConvert<F_iec685PDU<true>*>()){
      F_iec685PDU<true>* tempInfoPdu = qvariant_cast<F_iec685PDU<true>*>(messageField);

      tempInfoPdu->typeOfMessage      = REQUEST_INFO;
      tempInfoPdu->addr.ipSource      = infoPacket.addr.ipSource;
      tempInfoPdu->addr.ipDestination = infoPacket.addr.ipDestination;
      tempInfoPdu->idu.idEquipment    = infoPacket.idu.idEquipment;
      tempInfoPdu->idu.equipmentName  = "";
      tempInfoPdu->idu.ip             = infoPacket.idu.ip;
      tempInfoPdu->idu.areaName       = infoPacket.idu.areaName;
      tempInfoPdu->idu.type           = "";
 //  }

}

/**
 * @brief Initialyze data of tye Info to send to server
 * @details The messageField parameter help to hold both Information message and Command message
 * @param[in] messageField,
 * @param[in] commandPacket
 */
void ClientLogicEngine::initializeCommandMessageUnit(QVariant messageField,const F_iec685PDU<false> commandPacket){
    //if(messageField.canConvert<F_iec685PDU<false>*>()) {
          F_iec685PDU<false>* tempCmdPdu = qvariant_cast<F_iec685PDU<false>*>(messageField);

          tempCmdPdu->typeOfMessage       = REQUEST_CMD;
          tempCmdPdu->addr.ipSource       = commandPacket.addr.ipSource;
          tempCmdPdu->addr.ipDestination  = commandPacket.addr.ipDestination;

          tempCmdPdu->commandIdentifier   = COMMAND_OPEN_CMD;
     //  }
}

/**
 * @brief ClientLogicEngine::handleDataAvailableForUi
 * @details when data is received, a signal (DataAvailableForUi) is emitted to alert the UI.
 * But if whe want also do some action in this Logic Part File, we can use this slot
 * @return fields
 */
QStringList ClientLogicEngine::handleDataAvailableForUi()
{
    QStringList fields;
   // Do something
    qDebug()<<"slot engine";
    return  fields;
}

/**
 * @brief Handles the raising of ReadyRead Signal which is emitted when receive a message from server
 */
void ClientLogicEngine::onReadyRead()
{
   qDebug()<< "incoming call";

   QDataStream in(&socket);
   in.setVersion(QDataStream::Qt_5_10);

   QStringList fields;
   forever{
      if(nextBlockSize == 0) {
         if(socket.bytesAvailable() < sizeof(quint16))
            break;
         in >> nextBlockSize;
      }
      if( nextBlockSize == 0xFFFF){
         closeConnection();
         qDebug()<<"Connection Closed nextBSize=0xFFFF";
         break;
      }
      if (socket.bytesAvailable() < nextBlockSize ) break;

      int typeMessage = interpreteReceivedFlowAndCreatePacket(in);
      listEquipment = decodeReceivedPDUfromServer(messageField);

      if (typeMessage == REPLY_INFO)
         emit dataAvailableForUi();
   }
}

/**
 * @brief Establish connection and write the data to be send by socket
 * @details
 * @param messageFieldiant[in], hold F_iec685 info or command packet
 */
void ClientLogicEngine::sendRequest(const QVariant messageFieldiant)
{
   connectToServer();
   QByteArray block;
   QDataStream out(&block, QIODevice::WriteOnly);
   out.setVersion(QDataStream::Qt_5_10);

   if(messageFieldiant.canConvert<F_iec685PDU<true>*>()){
      F_iec685PDU<true>* tempInfoPdu = qvariant_cast<F_iec685PDU<true>*>(messageFieldiant);

      out << quint16(0)
          << tempInfoPdu->typeOfMessage
          << tempInfoPdu->addr.ipSource
          << tempInfoPdu->addr.ipDestination
          << tempInfoPdu->idu.idEquipment
          << tempInfoPdu->idu.equipmentName
          << tempInfoPdu->idu.ip
          << tempInfoPdu->idu.areaName
          << tempInfoPdu->idu.type;

      out.device()->seek(0);
      out << quint16(block.size() - sizeof(quint16));

   }
   if(messageFieldiant.canConvert<F_iec685PDU<false>*>()){
      F_iec685PDU<false>* tempCmdPdu = qvariant_cast<F_iec685PDU<false>*>(messageFieldiant);

      out << quint16(0)
          << tempCmdPdu->typeOfMessage
          << tempCmdPdu->addr.ipSource
          << tempCmdPdu->addr.ipDestination
          << tempCmdPdu->commandIdentifier;

      out.device()->seek(0);
      out << quint16(block.size() - sizeof(quint16));
   }

   socket.write(block);    //send data to server
   qDebug()<< "sending request to server...";
}

/**
 * @brief Interpretes received flow and creates Packet
 * @details Taking a QDataStream, make the F_iec685PDU packet with data coming from server within this stream
 * @param in[in, out] datastream with contains received flow
 * @return typeOfMessage(quint8)
 */
int ClientLogicEngine::interpreteReceivedFlowAndCreatePacket(QDataStream &in)
{
   quint8 typeOfMessage;
   in >> typeOfMessage;
   qDebug()<< "type message recu:" << typeOfMessage<<endl;

   if(typeOfMessage == REPLY_INFO){
      F_iec685PDU<true>* tempInfoPdu = new F_iec685PDU<true>();

      in >> tempInfoPdu->addr.ipSource
         >> tempInfoPdu->addr.ipDestination
         >> tempInfoPdu->idu.idEquipment
         >> tempInfoPdu->idu.equipmentName
         >> tempInfoPdu->idu.ip
         >> tempInfoPdu->idu.areaName
         >> tempInfoPdu->idu.type ;
            tempInfoPdu->typeOfMessage = typeOfMessage;

       messageField.setValue(tempInfoPdu);
   }
   if(typeOfMessage == REPLY_CMD){
      F_iec685PDU<false> * commandPDU = new F_iec685PDU<false>();

      in >> commandPDU->commandIdentifier;
            commandPDU->typeOfMessage = typeOfMessage;
      messageField.setValue(commandPDU);
   }
   return typeOfMessage;
}

/**
 * @brief Decode the packet and extract necessary fields in a list to send for processing
 * @param ReceivedIEC104PDU[in] packet in wich extract data from
 * @return QStringList with pertinent data
 */

QStringList ClientLogicEngine::decodeReceivedPDUfromServer(QVariant receivedIEC104PDU)
{
   QStringList fields;
   if(receivedIEC104PDU.canConvert<F_iec685PDU<true>*>()){

      F_iec685PDU<true> * infoPDU = qvariant_cast<F_iec685PDU<true>*>(receivedIEC104PDU);

      fields << QString::number(infoPDU->idu.idEquipment)
             << infoPDU->idu.equipmentName
             << infoPDU->idu.ip
             << infoPDU->idu.areaName
             << infoPDU->idu.type;
   }
   if(receivedIEC104PDU.canConvert<F_iec685PDU<false>*>()){
      F_iec685PDU<false> * commandPDU = qvariant_cast<F_iec685PDU<false>*>(receivedIEC104PDU);
      qDebug()<< "commandIdentifier received:"
              << static_cast<QString>(commandPDU->commandIdentifier)<<endl;
    }
      nextBlockSize = 0;
      return  fields;
}

/**
 * @brief Send data request for information data
 * @param[in] infoPacket , Packet to send
 */
void ClientLogicEngine::sendRequestInfo(F_iec685PDU<true> infoPacket)
{
    F_iec685PDU<true> *messageInfo = new F_iec685PDU<true>;
    messageField.setValue(messageInfo);
    initializeInfoMessageUnit(messageField, infoPacket);
    sendRequest(messageField);
}


/**
 * @brief Send data request command
 * @param[in] commandPacket, command packet to send
*/
void ClientLogicEngine::sendRequestCommand(F_iec685PDU<false> commandPacket)
{
    F_iec685PDU<false> *messageCommand = new F_iec685PDU<false>;
    messageField.setValue(messageCommand);
    initializeCommandMessageUnit(messageField, commandPacket);
    sendRequest(messageField);
}


//state
/**
 * @brief Handles the raising of Connected Signal which is emmited when connection is done
*/
void ClientLogicEngine::onConnected()
{
   qDebug()<<Q_FUNC_INFO << "Logic Engine: Client connected";
}

/**
 * @brief Close connection
*/
void ClientLogicEngine::closeConnection()
{
   socket.close();
   qDebug()<< "Connection closed";
}

/**
 * @brief Called when Search is stopped by user
*/
void ClientLogicEngine::stopSearch()
{
  qDebug()<< "Search Stopped";
  closeConnection();
}

/**
 * @brief Connection closed by Server
 * @details 0xFFFF is data send by server side to indicate end of his transmission
 */
void ClientLogicEngine::connectionClosedByServer()
{
   if( nextBlockSize != 0xFFFF){
       closeConnection();
       qDebug()<< "OK! Connection closed by server";
   }
}

/**
 * @brief Handles the raising of Error signal
*/

void ClientLogicEngine::error()
{
   qDebug()<< "Error, " << socket.errorString()<< endl;
   closeConnection();
}



