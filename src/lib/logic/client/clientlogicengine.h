/**
*@file clientlogicengine.h
*@brief declaration of class : ClientLogicEngine
*@date 10/2020
*@author Aymard Djasrabé M.
*/


#ifndef CLIENTLOGICENGINE_H
#define CLIENTLOGICENGINE_H

#include <memory>
#include <QCoreApplication>
#include <QDate>
#include <QDataStream>
#include <QDebug>
#include <QHostAddress>
#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QTime>
#include <QVariant>

#include "../protocole/messageunit.h"
#include "../protocole/pduconstvalues.h"

/**
 * @class ClientLogicEngine
 * @brief implements all the iec108 client's operating logic
 * @details
 */
class ClientLogicEngine : public QObject
{
   Q_OBJECT

public:
   explicit ClientLogicEngine();
   virtual ~ClientLogicEngine();
   inline QTcpSocket* getSocket(){ return &socket;}
   inline QStringList getListEquipment(){ return listEquipment;}

   //just checking Boost.Test
   inline int sum(int a, int b){return a + b;}

signals:
  QStringList dataAvailableForUi();

public slots:
   void connectToServer();
   void sendRequest(const QVariant qVariant);
   void onConnected();
   void onReadyRead();   
   void stopSearch();
   void connectionClosedByServer();
   void error();
   QStringList handleDataAvailableForUi();
   void sendRequestInfo(F_iec685PDU<true> infoPacket);
   void sendRequestCommand(F_iec685PDU<false> infoPacket);

private:
   void closeConnection();
   int interpreteReceivedFlowAndCreatePacket(QDataStream &in);
   QStringList decodeReceivedPDUfromServer(QVariant receivedIEC104PDU);
   void initializeInfoMessageUnit(QVariant messageField, const F_iec685PDU<true> infoPacket);
   void initializeCommandMessageUnit(QVariant messageField, const F_iec685PDU<false> commandPacket);


   QVariant messageField;      //!< holds both iec108 Info or Command message
   quint16 nextBlockSize;      //!< size of the next block to read
   QTcpSocket socket;          //!< Socket use to keep exchange
   QStringList listEquipment;  //!< Holds list of equipments to be send to ui

};
Q_DECLARE_METATYPE(ClientLogicEngine*)
#endif // CLIENTLOGICENGINE_H
