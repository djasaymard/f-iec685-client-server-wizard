/**
*@file clienthandlersocket.h
*@brief declaration of class : ClientHandlerSocket
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#ifndef CLIENTHANDLERSOCKET_H
#define CLIENTHANDLERSOCKET_H

#include <QDataStream>
#include <QDate>
#include <QObject>
#include <QSqlError>
#include <QSqlQuery>
#include <QTcpSocket>
#include <QThread>
#include <QTime>

#include "../../data/databaseconnection.h"
#include "../protocole/messageunit.h"
#include "../protocole/pduconstvalues.h"

/**
 * @class ClientHandlerSocket
 * @brief implements logic of handle all incoming client's request
*/
class ClientHandlerSocket : public QThread{
   Q_OBJECT
public:
   explicit ClientHandlerSocket(qintptr descriptor, QObject *prent = nullptr);
   virtual ~ClientHandlerSocket();

    //just checking Boost.Test
   inline int sum(int a, int b){return a + b;}

private slots:
   void onReadyRead();
   void onConnected();
   void onDisconnected();
   void error();

protected:
   void run() Q_DECL_OVERRIDE;

private:
   void getRequestedDataInDatabase(F_iec685PDU<true> *infoPDU);
   void decodeReceivedPDUfromClient(QVariant packetToInterpret);
   void interpreteReceivedFlowAndCreatePacket(QDataStream &in);
   void respondToClientInfoRequest(F_iec685PDU<true> * tempInfoPdu);
   void respondToClientCommandRequest(F_iec685PDU<false> * tempInfoPdu);
   void initializeInfoMessageUnit(F_iec685PDU<true>* infoPDU, const F_iec685PDU<true> infoPacketTosend);


   quint16 nextBlockSize; //!< size of the next block to read
   QTcpSocket *socket;    //!< Socket use to keep exchange
   qintptr  descriptor;   //!< Native socket descriptor for the accepted connection.
   QVariant messageField; //!< hold both F_iec685 Info or command message

};

#endif // CLIENTHANDLERSOCKET_H
