/**
*@file server.cpp
*@brief Implementation of class : Server
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#include "server.h"

Server::Server(QObject *parent): QTcpServer(parent)
{
}

Server::~Server()
{
}

/**
 * @brief start server and listen to the specified port
 */
void Server::initServer()
{
   if(this->listen(QHostAddress::Any, 4242)){
      qDebug()<<"Server started and listening at port" << this->serverPort();
   }
   else{
      qDebug()<<"Starting server failure";
   }
}

/**
 * @brief handles incoming Connection
 * @details This virtual function is called by QTcpServer when a
 *  new connection is available. The socketDescriptor argument
 *  is the native socket descriptor for the accepted connection.
 * @param[in] socketId
 */
void Server::incomingConnection(qintptr socketId)
{
   qDebug()<< "New connection detected";
   ClientHandlerSocket *socket = new ClientHandlerSocket(socketId);
   //socket->setSocketDescriptor(socketId);
   connect(socket, SIGNAL(finished()), socket, SLOT(deleteLater()));
   socket->start();
}


