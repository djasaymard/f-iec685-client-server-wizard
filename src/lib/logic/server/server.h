/**
*@file server.h
*@brief Declaration of class : Server
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#ifndef SERVER_H
#define SERVER_H

#include <QAbstractSocket>
#include <QDebug>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

#include "clienthandlersocket.h"

/**
 * @class Server
 * @brief Manage server side thanks to logic in ClienHandlerSocket
 */
class Server : public QTcpServer
{
   Q_OBJECT
public:
   Server(QObject *parent = nullptr);
   ~Server() override;
   void initServer();
   void incomingConnection(qintptr socketId) Q_DECL_OVERRIDE;
};
#endif // SERVER_H
