/**
*@file clienthandlersocket.cpp
* @brief implementation of class : ClientHandlerSocket
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#include "clienthandlersocket.h"

/**
 * @brief Constructor
*/
ClientHandlerSocket::ClientHandlerSocket(qintptr descriptor, QObject *parent):
   QThread(parent), descriptor(descriptor)
{
   messageField.setValue(nullptr);
   nextBlockSize = 0;
}

/**
 * @brief called just after thread started
*/
void ClientHandlerSocket::run()
{
   qDebug()<< Q_FUNC_INFO;
   socket = new QTcpSocket;
   socket->setSocketDescriptor(descriptor);

   DataBaseConnection db;

   connect(socket, SIGNAL(readyRead()),    this, SLOT(onReadyRead()), Qt::DirectConnection);
   connect(socket, SIGNAL(disconnected()), this, SLOT(onDisconnected()), Qt::DirectConnection);
   connect(socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error()));
   exec();
}

ClientHandlerSocket::~ClientHandlerSocket()
{

}

/**
 * @brief InterpreteReceivedFlowAndCreatePacket
 * @details take a QDataStream, make the F_iec685 packet with data coming from client within the stream
 * @param in[in, out]
 */
void ClientHandlerSocket::interpreteReceivedFlowAndCreatePacket(QDataStream &in)
{
   quint8 temptypeOfMessage;

   in >> temptypeOfMessage;
   qDebug() << static_cast<QString>(temptypeOfMessage);

   if(temptypeOfMessage == REQUEST_INFO){
      F_iec685PDU<true>* tempInfoPdu = new F_iec685PDU<true>();

      in >> tempInfoPdu->addr.ipSource
         >> tempInfoPdu->addr.ipDestination
         >> tempInfoPdu->idu.idEquipment
         >> tempInfoPdu->idu.equipmentName
         >> tempInfoPdu->idu.ip
         >> tempInfoPdu->idu.areaName
         >> tempInfoPdu->idu.type;
            tempInfoPdu->typeOfMessage = temptypeOfMessage;

       //for visual checking only, to delete
         qDebug()<< "Data Info received from client";
         qDebug() << nextBlockSize;
         qDebug() << static_cast<QString>(temptypeOfMessage);
         qDebug() << tempInfoPdu->addr.ipSource;
         qDebug() << tempInfoPdu->addr.ipDestination;
         qDebug() << tempInfoPdu->idu.idEquipment;
         qDebug() << tempInfoPdu->idu.equipmentName;
         qDebug() << tempInfoPdu->idu.ip;
         qDebug() << tempInfoPdu->idu.areaName;
         qDebug() << tempInfoPdu->idu.type<<endl;

         messageField.setValue(tempInfoPdu);
   }

   if(temptypeOfMessage == REQUEST_CMD){
      F_iec685PDU<false>* tempCommandPdu = new F_iec685PDU<false>() ;

      in >> tempCommandPdu->addr.ipSource
         >> tempCommandPdu->addr.ipDestination
         >> tempCommandPdu->commandIdentifier;
            tempCommandPdu->typeOfMessage = temptypeOfMessage;
      messageField.setValue(tempCommandPdu);

      //for visual checking only,
      qDebug()<< "Data comman received from client";
      qDebug() << nextBlockSize;
      qDebug() << static_cast<QString>(temptypeOfMessage);
      qDebug() << static_cast<QString>(tempCommandPdu->addr.ipSource);
      qDebug() << static_cast<QString>(tempCommandPdu->addr.ipDestination);
      qDebug() << static_cast<QString>(tempCommandPdu->commandIdentifier);

      messageField.setValue(tempCommandPdu);
   }
}

/**
 * @brief Decode the packet and extract necessary fields for processing
 * @param ReceivedIEC104PDU[in] packet in wich extract data from
 */
void ClientHandlerSocket::decodeReceivedPDUfromClient(QVariant packetToInterpret)
{
   if(packetToInterpret.canConvert<F_iec685PDU<true>*>()){
      F_iec685PDU<true>* tempInfoPdu = qvariant_cast<F_iec685PDU<true>*>(packetToInterpret);
      getRequestedDataInDatabase(tempInfoPdu);
   }
   if(packetToInterpret.canConvert<F_iec685PDU<false>*>()){
      F_iec685PDU<false>* tempCommandPdu = qvariant_cast<F_iec685PDU<false>*>(packetToInterpret);
      //here extract some fields, do some actions if necessary before respond to client
      respondToClientCommandRequest(tempCommandPdu);
   }
}

/**
 * @brief Handles the raising of ReadyRead Signal which is emitted when receive a message from client
*/
void ClientHandlerSocket::onReadyRead()
{
   qDebug()<< " Receiving data from client:";
   QDataStream in(socket);
   in.setVersion(QDataStream::Qt_5_10);

   if(nextBlockSize ==0){
      if(socket->bytesAvailable() < sizeof (quint16))
        return;
      in >> nextBlockSize;
   }
   if(socket->bytesAvailable() < nextBlockSize)return;
      //Decode the packet
      interpreteReceivedFlowAndCreatePacket(in);
      decodeReceivedPDUfromClient(messageField);

      //flag to close connection
      QDataStream out(socket);
      out<< quint16(0xFFFF);

      socket->close();
      qDebug()<<"Connexion closed";
}

/**
 * @brief Define data to send like response to client Info Request
 * @details
 * @param[out] infoPDU , F_iec685 Info packet
 */
void ClientHandlerSocket::initializeInfoMessageUnit(F_iec685PDU<true>* infoPDU,  const F_iec685PDU<true> infoPacketTosend)

{
   infoPDU->typeOfMessage      = REPLY_INFO;
   infoPDU->addr.ipSource      = "192.168.1.3";
   infoPDU->addr.ipDestination = "192.168.1.2";

   infoPDU->idu.idEquipment    = infoPacketTosend.idu.idEquipment;
   infoPDU->idu.equipmentName  = infoPacketTosend.idu.equipmentName;
   infoPDU->idu.ip             = infoPacketTosend.idu.ip;
   infoPDU->idu.areaName       = infoPacketTosend.idu.areaName;
   infoPDU->idu.type           = infoPacketTosend.idu.type;
 }

/**
 * @brief Write the data(Information Packet) to be send to client in the output stream
 * @param[in]tempInfoPdu,
 */
void ClientHandlerSocket::respondToClientInfoRequest(F_iec685PDU<true> * tempInfoPdu)
{
   QByteArray block;
   QDataStream out(&block, QIODevice::WriteOnly);
   out.setVersion(QDataStream::Qt_5_10);

   out << quint16(0)
       << tempInfoPdu->typeOfMessage
       << tempInfoPdu->addr.ipSource
       << tempInfoPdu->addr.ipDestination
       << tempInfoPdu->idu.idEquipment
       << tempInfoPdu->idu.equipmentName
       << tempInfoPdu->idu.ip
       << tempInfoPdu->idu.areaName
       << tempInfoPdu->idu.type;

   out.device()->seek(0);
   out << quint16(block.size() - sizeof (quint16));

   socket->write(block);
   qDebug()<< "responding to client ";
}

/**
 * @brief Write the data(Information Packet) to be send to client in the output stream
 * @param[in] tempInfoPdu
 */
void ClientHandlerSocket::respondToClientCommandRequest(F_iec685PDU<false> * tempInfoPdu)
{
   QByteArray block;
   QDataStream out(&block, QIODevice::WriteOnly);
   out.setVersion(QDataStream::Qt_5_10);

   quint8 typeOfMessage = REPLY_CMD;
   quint16 commandIdentifier = COMMAND_STATUT_OK_REPLY;  //controle of execution must be done before

   out << quint16(0)
       << typeOfMessage
       << commandIdentifier;

   out.device()->seek(0);
   out << quint16(block.size() - sizeof (quint16));

   socket->write(block);
}

/**
 * @brief ClientHandlerSocket::getRequestedDataInDatabase
 * @details
 * @param[in] infoPDU containst data used to filter the SQL request
 * @todo add method for setting a query in the databaseconnection class
 */
void ClientHandlerSocket::getRequestedDataInDatabase(F_iec685PDU<true> *infoPDU)
{
   QString sql = "select EQUIPMENT.idEquipment, EQUIPMENT.name, EQUIPMENT.ip, AREA.name, MODELE.type "
                    "from EQUIPMENT, AREA, MODELE "
                    "WHERE EQUIPMENT.ip =? AND EQUIPMENT.idArea = AREA.idArea AND EQUIPMENT.idModele = MODELE.idModele "
                    "ORDER BY EQUIPMENT.idEquipment";
   QSqlQuery query;
   query.prepare(sql);
   query.bindValue(0, infoPDU->idu.ip);
   query.exec();

   F_iec685PDU<true> infoPacketTosend;
   while(query.next()){
       infoPacketTosend.idu.idEquipment   = query.value(0).toInt();
          infoPacketTosend.idu.equipmentName = query.value(1).toString();
          infoPacketTosend.idu.ip            = query.value(2).toString();
          infoPacketTosend.idu.areaName      = query.value(3).toString();
          infoPacketTosend.idu.type          = query.value(4).toString();

          qDebug()<< infoPacketTosend.idu.idEquipment << infoPacketTosend.idu.equipmentName
                  << infoPacketTosend.idu.ip<< infoPacketTosend.idu.areaName << infoPacketTosend.idu.type << endl;

          initializeInfoMessageUnit(infoPDU, infoPacketTosend);
          respondToClientInfoRequest(infoPDU);
   }
}

/**
 * @brief Handles the raising of Error Signal
*/
void ClientHandlerSocket::error()
{
    qDebug()<< "Error" << socket->errorString()<< endl;
}

/**
 * @brief Handles the raising of Disconnect Signal
*/
void ClientHandlerSocket::onDisconnected()
{
   socket->close();
   quit();
}

/**
 * @brief Handles the raising of Connected Signal
*/
void ClientHandlerSocket::onConnected()
{
   qDebug()<< "Client connected";
}

