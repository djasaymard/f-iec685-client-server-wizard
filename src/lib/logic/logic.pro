#-------------------------------------------------
#
# Project created by QtCreator 2020-11-11T17:23:10
#
#-------------------------------------------------
# Check if the config file exists
! include( ../../../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

QT += network
QT += core
QT += sql

CONFIG += c++14


# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
           client/clientlogicengine.h\
           server/clienthandlersocket.h \
           server/server.h \
           protocole/messageunit.h \
           protocole/pduconstvalues.h

SOURCES += \
           client/clientlogicengine.cpp\
           server/clienthandlersocket.cpp\
           server/server.cpp

LIBS  += -L../data -ldata


