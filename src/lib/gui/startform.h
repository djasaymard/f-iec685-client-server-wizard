/**
*@file startform.h
*@brief Declaration of class : StartForm
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#ifndef STARTFORM_H
#define STARTFORM_H

#include <QDebug>
#include <memory>
#include <QMessageBox>
#include <QWidget>

#include "client.h"
#include "../logic/server/server.h"

namespace Ui {
class StartForm;
}

/**
 * @class StartForm
 * @brief manage ui wich allows to start the App as server or as client
 */
class StartForm : public QWidget
{
   Q_OBJECT

public:
    explicit StartForm(QWidget *parent = nullptr);
    ~StartForm();

private slots:
    void on_ButtonServerStart_clicked();
    void on_ButtonClientStart_clicked();

private:
    bool serverState = false;
    bool clientState = false;
    bool isServerStarted(){return serverState;}
    bool isClientStarted(){return clientState;}

    Ui::StartForm *ui;

    std::unique_ptr<Server> server;  //!< A server instance to start
    std::unique_ptr<Client> client;  //!< A client instance to start

};
#endif // STARTFORM_H
