/********************************************************************************
** Form generated from reading UI file 'startform.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STARTFORM_H
#define UI_STARTFORM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_StartForm
{
public:
    QGroupBox *GroupBox;
    QPushButton *ButtonServerStart;
    QPushButton *ButtonClientStart;
    QLabel *label;

    void setupUi(QWidget *StartForm)
    {
        if (StartForm->objectName().isEmpty())
            StartForm->setObjectName(QLatin1String("StartForm"));
        StartForm->resize(320, 249);
        GroupBox = new QGroupBox(StartForm);
        GroupBox->setObjectName(QLatin1String("GroupBox"));
        GroupBox->setGeometry(QRect(70, 50, 231, 161));
        ButtonServerStart = new QPushButton(GroupBox);
        ButtonServerStart->setObjectName(QLatin1String("ButtonServerStart"));
        ButtonServerStart->setGeometry(QRect(90, 60, 131, 27));
        ButtonClientStart = new QPushButton(GroupBox);
        ButtonClientStart->setObjectName(QLatin1String("ButtonClientStart"));
        ButtonClientStart->setGeometry(QRect(90, 110, 131, 27));
        label = new QLabel(StartForm);
        label->setObjectName(QLatin1String("label"));
        label->setGeometry(QRect(10, 10, 111, 19));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        retranslateUi(StartForm);

        QMetaObject::connectSlotsByName(StartForm);
    } // setupUi

    void retranslateUi(QWidget *StartForm)
    {
        StartForm->setWindowTitle(QApplication::translate("StartForm", "Form", nullptr));
        GroupBox->setTitle(QApplication::translate("StartForm", "Start ", nullptr));
        ButtonServerStart->setText(QApplication::translate("StartForm", "Start as Server", nullptr));
        ButtonClientStart->setText(QApplication::translate("StartForm", "Start as Client", nullptr));
        label->setText(QApplication::translate("StartForm", "Application", nullptr));
    } // retranslateUi

};

namespace Ui {
    class StartForm: public Ui_StartForm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STARTFORM_H
