/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionClientServer;
    QAction *actiontest;
    QAction *actiontest2;
    QAction *actionQuit;
    QAction *actionUsers;
    QAction *actionAdd_Users;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuFichier;
    QMenu *menuStart;
    QMenu *menuManagement;
    QMenu *menuUsers;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QLatin1String("MainWindow"));
        MainWindow->resize(468, 342);
        actionClientServer = new QAction(MainWindow);
        actionClientServer->setObjectName(QLatin1String("actionClientServer"));
        actiontest = new QAction(MainWindow);
        actiontest->setObjectName(QLatin1String("actiontest"));
        actiontest2 = new QAction(MainWindow);
        actiontest2->setObjectName(QLatin1String("actiontest2"));
        actionQuit = new QAction(MainWindow);
        actionQuit->setObjectName(QLatin1String("actionQuit"));
        actionUsers = new QAction(MainWindow);
        actionUsers->setObjectName(QLatin1String("actionUsers"));
        actionAdd_Users = new QAction(MainWindow);
        actionAdd_Users->setObjectName(QLatin1String("actionAdd_Users"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QLatin1String("centralWidget"));
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QLatin1String("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 468, 24));
        menuFichier = new QMenu(menuBar);
        menuFichier->setObjectName(QLatin1String("menuFichier"));
        menuStart = new QMenu(menuBar);
        menuStart->setObjectName(QLatin1String("menuStart"));
        menuManagement = new QMenu(menuBar);
        menuManagement->setObjectName(QLatin1String("menuManagement"));
        menuUsers = new QMenu(menuBar);
        menuUsers->setObjectName(QLatin1String("menuUsers"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QLatin1String("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QLatin1String("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFichier->menuAction());
        menuBar->addAction(menuStart->menuAction());
        menuBar->addAction(menuManagement->menuAction());
        menuBar->addAction(menuUsers->menuAction());
        menuFichier->addAction(actiontest);
        menuFichier->addSeparator();
        menuFichier->addSeparator();
        menuFichier->addAction(actiontest2);
        menuFichier->addAction(actionQuit);
        menuStart->addAction(actionClientServer);
        menuUsers->addSeparator();
        menuUsers->addAction(actionUsers);
        menuUsers->addAction(actionAdd_Users);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionClientServer->setText(QApplication::translate("MainWindow", "Client/Server", nullptr));
        actiontest->setText(QApplication::translate("MainWindow", "test", nullptr));
        actiontest2->setText(QApplication::translate("MainWindow", "test2", nullptr));
        actionQuit->setText(QApplication::translate("MainWindow", "Quit", nullptr));
        actionUsers->setText(QApplication::translate("MainWindow", "List of USers", nullptr));
        actionAdd_Users->setText(QApplication::translate("MainWindow", "Add Users", nullptr));
        menuFichier->setTitle(QApplication::translate("MainWindow", "Fichier", nullptr));
        menuStart->setTitle(QApplication::translate("MainWindow", "Start", nullptr));
        menuManagement->setTitle(QApplication::translate("MainWindow", "Management", nullptr));
        menuUsers->setTitle(QApplication::translate("MainWindow", "Users", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
