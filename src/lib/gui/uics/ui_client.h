/********************************************************************************
** Form generated from reading UI file 'client.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENT_H
#define UI_CLIENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Client
{
public:
    QGroupBox *groupBox;
    QComboBox *comboboxArea;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *ipEdit;
    QPushButton *buttonIdEquipment;
    QLineEdit *idEquipmentEdit;
    QTableWidget *tableWidget;
    QPushButton *buttonSearch;
    QPushButton *buttonQuit;
    QPushButton *buttonStop;
    QProgressBar *progressBar;
    QLabel *labelStatut;
    QPushButton *buttonCommand1;

    void setupUi(QWidget *Client)
    {
        if (Client->objectName().isEmpty())
            Client->setObjectName(QLatin1String("Client"));
        Client->resize(723, 534);
        groupBox = new QGroupBox(Client);
        groupBox->setObjectName(QLatin1String("groupBox"));
        groupBox->setGeometry(QRect(20, 50, 511, 151));
        comboboxArea = new QComboBox(groupBox);
        comboboxArea->addItem(QString());
        comboboxArea->addItem(QString());
        comboboxArea->addItem(QString());
        comboboxArea->setObjectName(QLatin1String("comboboxArea"));
        comboboxArea->setGeometry(QRect(300, 110, 191, 27));
        label = new QLabel(groupBox);
        label->setObjectName(QLatin1String("label"));
        label->setGeometry(QRect(30, 40, 67, 19));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QLatin1String("label_2"));
        label_2->setGeometry(QRect(20, 120, 67, 19));
        ipEdit = new QLineEdit(groupBox);
        ipEdit->setObjectName(QLatin1String("ipEdit"));
        ipEdit->setGeometry(QRect(300, 30, 191, 27));
        buttonIdEquipment = new QPushButton(groupBox);
        buttonIdEquipment->setObjectName(QLatin1String("buttonIdEquipment"));
        buttonIdEquipment->setGeometry(QRect(20, 80, 88, 27));
        idEquipmentEdit = new QLineEdit(groupBox);
        idEquipmentEdit->setObjectName(QLatin1String("idEquipmentEdit"));
        idEquipmentEdit->setGeometry(QRect(300, 70, 191, 27));
        tableWidget = new QTableWidget(Client);
        if (tableWidget->columnCount() < 5)
            tableWidget->setColumnCount(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        tableWidget->setObjectName(QLatin1String("tableWidget"));
        tableWidget->setGeometry(QRect(20, 230, 511, 211));
        buttonSearch = new QPushButton(Client);
        buttonSearch->setObjectName(QLatin1String("buttonSearch"));
        buttonSearch->setGeometry(QRect(450, 450, 88, 27));
        buttonQuit = new QPushButton(Client);
        buttonQuit->setObjectName(QLatin1String("buttonQuit"));
        buttonQuit->setGeometry(QRect(640, 500, 71, 27));
        buttonStop = new QPushButton(Client);
        buttonStop->setObjectName(QLatin1String("buttonStop"));
        buttonStop->setGeometry(QRect(620, 190, 88, 27));
        progressBar = new QProgressBar(Client);
        progressBar->setObjectName(QLatin1String("progressBar"));
        progressBar->setGeometry(QRect(10, 10, 118, 23));
        progressBar->setValue(24);
        labelStatut = new QLabel(Client);
        labelStatut->setObjectName(QLatin1String("labelStatut"));
        labelStatut->setGeometry(QRect(0, 490, 291, 19));
        QFont font;
        font.setPointSize(8);
        labelStatut->setFont(font);
        buttonCommand1 = new QPushButton(Client);
        buttonCommand1->setObjectName(QLatin1String("buttonCommand1"));
        buttonCommand1->setGeometry(QRect(320, 450, 111, 27));

        retranslateUi(Client);

        QMetaObject::connectSlotsByName(Client);
    } // setupUi

    void retranslateUi(QWidget *Client)
    {
        Client->setWindowTitle(QApplication::translate("Client", "Form", nullptr));
        groupBox->setTitle(QApplication::translate("Client", "Trouver un equipement", nullptr));
        comboboxArea->setItemText(0, QApplication::translate("Client", "Orsay", nullptr));
        comboboxArea->setItemText(1, QApplication::translate("Client", "Massy", nullptr));
        comboboxArea->setItemText(2, QApplication::translate("Client", "Paris", nullptr));

        label->setText(QApplication::translate("Client", "ip", nullptr));
        label_2->setText(QApplication::translate("Client", "Area", nullptr));
        ipEdit->setText(QString());
        buttonIdEquipment->setText(QApplication::translate("Client", "id Equipment", nullptr));
        idEquipmentEdit->setText(QString());
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("Client", "idEquipment", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("Client", "Name", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("Client", "ip adresse", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("Client", "Area", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QApplication::translate("Client", "Type", nullptr));
        buttonSearch->setText(QApplication::translate("Client", "Search", nullptr));
        buttonQuit->setText(QApplication::translate("Client", "Quit", nullptr));
        buttonStop->setText(QApplication::translate("Client", "Stop", nullptr));
        labelStatut->setText(QApplication::translate("Client", "TextLabel", nullptr));
        buttonCommand1->setText(QApplication::translate("Client", "Command 1", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Client: public Ui_Client {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENT_H
