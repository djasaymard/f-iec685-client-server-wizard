/**
* @file mainwindow.cpp
* @brief Implementation of class : MainWindow
* @date 10/2020
* @author Aymard Djasrabé M.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
   ui->setupUi(this);
   ui->actionQuit->connect(ui->actionQuit, SIGNAL(triggered()), qApp, SLOT(quit()));
}

MainWindow::~MainWindow()
{
   delete ui;
}

/**
 * @brief Open Start form ui
*/
void MainWindow::on_actionClientServer_triggered()
{
   StartForm *startFormInstance = new StartForm;
   startFormInstance->show();
}

void MainWindow::on_actionQuit_triggered()
{

}


