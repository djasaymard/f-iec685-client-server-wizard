/**
* @file client.h
* @brief declaration of class : Client
* @date 10/2020
* @author Aymard Djasrabé M.
*/

#ifndef CLIENT_H
#define CLIENT_H

#include <QDate>
#include <QDebug>
#include <memory>
#include <QObject>
#include <QString>
#include <QTableWidget>
#include <QTime>
#include <QVariant>
#include <QWidget>

#include "../logic/client/clientlogicengine.h"
#include "../lib/logic/client/clientlogicengine.h"
#include "../lib/logic/protocole/messageunit.h"

class QPushButton;

namespace Ui {
   class Client;
}

/**
 * @class Client
 * @brief Manage client user interface by using the client's operating logic defined in clientlogicengine.h
 * @details
 */
class Client : public QWidget{

   Q_OBJECT

public:
   explicit Client(QWidget *parent = nullptr);
   ~Client();

private slots:
   void onReadyRead();
   void connectToServer();
   void stopSearch();
   void connectionClosedByServer();
   void error();
   void onConnected();
   void sendRequestInfo();
   void sendRequestCommand();
private:
   void updateEquipmentListTable(const QStringList);

   Ui::Client *ui;                                    //!< A client form
   std::unique_ptr<ClientLogicEngine> F_iec685Engine; //!< An instance of the class in wich all the logic are implemented

};
Q_DECLARE_METATYPE(Client*)

#endif // CLIENT_H
