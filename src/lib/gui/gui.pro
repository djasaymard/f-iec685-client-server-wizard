#-------------------------------------------------
#
# Project created by QtCreator 2020-11-12T02:09:45
#
#-------------------------------------------------
# Check if the config file exists
! include( ../../../common.pri ) {
    error( "Couldn't find the common.pri file!" )
}

CONFIG += c++14
QT += network
QT += core gui sql



greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#TARGET = gui
#TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0



SOURCES += \
        mainwindow.cpp\
        startform.cpp\
        client.cpp

HEADERS += \
        mainwindow.h\
        startform.h\
        client.h

FORMS += \
        forms/mainwindow.ui\
        forms/startform.ui\
        forms/client.ui

LIBS += -L../logic -llogic


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
