/**
* @file client.cpp
* @brief Implementation of class : Client
* @date 10/2020
* @author Aymard Djasrabé M.
*/

#include "ui_client.h"


#include "client.h"


Client::Client(QWidget *parent):
               QWidget(parent), ui(new Ui::Client)
{
   ui->setupUi(this);
   F_iec685Engine = std::make_unique<ClientLogicEngine>();


   connect(F_iec685Engine->getSocket(), SIGNAL(connected()), this, SLOT(onConnected()));
   connect(F_iec685Engine->getSocket(), SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error()));
   connect(F_iec685Engine->getSocket(), SIGNAL(disconnected()), this, SLOT(connectionClosedByServer()));

   connect(ui->buttonSearch,   SIGNAL(clicked()), this, SLOT(sendRequestInfo()));
   connect(ui->buttonCommand1, SIGNAL(clicked()), this, SLOT(sendRequestCommand()));
   connect(ui->buttonStop,     SIGNAL(clicked()), this, SLOT(stopSearch()));

   connect(F_iec685Engine.get(), SIGNAL(dataAvailableForUi()), this, SLOT(onReadyRead()));//signal declenched when receive signal from logic

   ui->buttonStop->setEnabled(true);

   ui->progressBar->hide();
   ui->progressBar->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Ignored);
   ui->tableWidget->verticalHeader()->hide();
   ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

Client::~Client(){
   delete ui;
}

void Client::connectToServer()
{
   F_iec685Engine->connectToServer();
   ui->tableWidget->clear();
   ui->tableWidget->setRowCount(0);
   ui->labelStatut->setText(tr("Connecting to server"));
}

void Client::onConnected()
{
   ui->labelStatut->setText(tr("Client connected"));
   F_iec685Engine->onConnected();
}

void Client::stopSearch()
{
   ui->labelStatut->setText(tr("Search Stopped"));
   F_iec685Engine->stopSearch();
}

void Client::connectionClosedByServer()
{
   ui->labelStatut->setText(tr("OK! Connection closed by server"));
   F_iec685Engine->connectionClosedByServer();
}

void Client::error()
{
   ui->labelStatut->setText(F_iec685Engine->getSocket()->errorString());
   F_iec685Engine->error();
}

void Client::onReadyRead()
{
   for (auto element: F_iec685Engine->getListEquipment()){
      qDebug()<< "data received"<<endl;
      qDebug()<< element << endl;
   }
   updateEquipmentListTable(F_iec685Engine->getListEquipment());
}


/**
 * @brief fill the table with data
 * @details fill the table with data returned by server and puted in QstingList
 * @param[in] fields
 */
void Client::updateEquipmentListTable(const QStringList fields)
{
   int row = ui->tableWidget->rowCount();
   Client::ui->tableWidget->setRowCount(row +1);

   for(int i = 0;  i<fields.count(); ++i){
       ui->tableWidget->setItem(row, i,
                               new QTableWidgetItem(fields [i]));
   }
}

/**
 * @brief Client::send data request for information
 * @todo ip adress in Text input widget
 */
void Client::sendRequestInfo()
{
   F_iec685PDU<true> infoPacket;
   infoPacket.addr.ipSource      = "192.168.1.3";
   infoPacket.addr.ipDestination = "192.168.1.2";
   infoPacket.idu.idEquipment    = (ui->buttonIdEquipment->text()).toInt();
   infoPacket.idu.ip             =  ui->ipEdit->text(),
   infoPacket.idu.areaName       =  ui->comboboxArea->currentText();

   F_iec685Engine->sendRequestInfo(infoPacket);
   ui->labelStatut->setText("sending Infomation request ....");
}




/**
 * @brief  @brief Client::send data request command
 */
void Client::sendRequestCommand()
{
    F_iec685PDU<false> commandPacket;
    commandPacket.addr.ipSource      = "192.168.1.3";
    commandPacket.addr.ipDestination = "192.168.1.2";

    F_iec685Engine->sendRequestCommand(commandPacket);
   ui->labelStatut->setText("sending command request ....");
}





