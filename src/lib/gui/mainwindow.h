/**
*@file mainwindow.h
*@brief Declaration of class : MainWindow
*@date 10/2020
*@author Aymard Djasrabé M.
*/


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "startform.h"

namespace Ui {
   class MainWindow;
}

/**
 * @class MainWindow
 * @brief Manage main user interface off the App
 */
class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow(QWidget *parent = nullptr);
   ~MainWindow();

private slots:
   void on_actionQuit_triggered();
   void on_actionClientServer_triggered();

private:
   Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
