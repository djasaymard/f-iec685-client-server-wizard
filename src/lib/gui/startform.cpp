/**
*@file startform.cpp
*@brief Implementation of class : StartForm
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#include "startform.h"
#include "ui_startform.h"

/**
 * @brief Constructor
 * @param parent
 */
StartForm::StartForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StartForm)
{
   ui->setupUi(this);
}

StartForm::~StartForm()
{
   delete ui;
}

/**
 * @brief Start the App as f-iec685 Server
 */
void StartForm::on_ButtonServerStart_clicked()
{
   clientState = false;
   serverState = true;
   server = std::make_unique<Server>();
   server->initServer();
   QMessageBox::information(this,"Server Start", "Server Start");
}


/**
 * @brief Start the App as f-iec685 Client
 */
void StartForm::on_ButtonClientStart_clicked()
{
   serverState = false;
   clientState = true;
   client = std::make_unique<Client>();
   (*client).show();
   QMessageBox::information(this,"Client Start", "Client Start");
}
