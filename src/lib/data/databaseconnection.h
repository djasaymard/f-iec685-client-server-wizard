/**
*@file databaseconnection.h
*@brief declaration of class : dataBaseConnection
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#ifndef DATABASECONNECTION_H
#define DATABASECONNECTION_H

#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QtSql/QSqlDatabase>

/**
 * @class DataBaseConnection
 * @brief Provides access to database
 * @todo  Complete the class
*/
class DataBaseConnection
{
public:
    DataBaseConnection();
    QSqlDatabase db;
    QSqlQuery query;
};

#endif // DATABASECONNECTION_H
