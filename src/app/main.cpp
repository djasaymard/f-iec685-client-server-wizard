/**
*@file main.cpp
*@brief Entry point of the App
*@date 10/2020
*@author Aymard Djasrabé M.
*/

#include <QApplication>

#include  <../lib/gui/mainwindow.h>

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
