
------------------------------------
-- Create Database 
------------------------------------
CREATE Database controleDB CHARACTER SET 'utf8';

------------------------------------
-- Tables creation
------------------------------------
CREATE TABLE AREA( 
                    idArea SMALLINT NOT NULL, 
                    name varchar(50),
                    size int,
                    responsibilty varchar(30),   
                    PRIMARY KEY(idArea) 
                 )  ENGINE=INNODB;

CREATE TABLE MODELE(
                   idModele int not null, 
                   type varchar(30),
                   PRIMARY KEY(idModele)
                   ) ENGINE =INNODB;


CREATE TABLE EQUIPMENT(
                       idEquipment int not null, 
                       name varchar(50), 
                       ip varchar(25), 
                       idArea SMALLINT,
                       idModele int,
                       PRIMARY KEY(idEquipment), 
                       FOREIGN KEY (idArea)   REFERENCES AREA(idAREA), 
                                   (idModele) REFERENCES MODELE(idMODELE)
                       ) ENGINE = INNODDB; 

------------------------------------
--  Insertion data
------------------------------------
insert into MODELE(idModele, type) 
            VALUES(1, 'PC');
insert into AREA(idArea, name,size,responsibility)
            VALUES(2,'Orsay', '25', 'Mbangossoum');
insert into EQUIPMENT(idEquipment, name, ip, idArea, idModele)
            VALUES (1,'pcAlpha', '192.168.1.2', 1, 1);

------------------------------------
-- Exemple of request
------------------------------------
SELECT
      EQUIPMENT.idEquipment, EQUIPMENT.name, EQUIPMENT.ip, 
      AREA.name, MODELE.type 
 FROM  EQUIPMENT, AREA, MODELE
 WHERE EQUIPMENT.idArea = AREA.idArea AND
       EQUIPMENT.idModele = MODELE.idModele 
