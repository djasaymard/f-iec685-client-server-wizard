var searchData=
[
  ['sendrequest',['sendRequest',['../class_client_logic_engine.html#ad30f486e7449eb99fa9289115dbfbff0',1,'ClientLogicEngine']]],
  ['sendrequestcommand',['sendRequestCommand',['../class_client.html#af73f84d5c2a86451edc33e29903fa007',1,'Client::sendRequestCommand()'],['../class_client_logic_engine.html#ace1e702a41758be0308474b8c1e4b214',1,'ClientLogicEngine::sendRequestCommand()']]],
  ['sendrequestinfo',['sendRequestInfo',['../class_client.html#a9880e1095f64455f0d9e33d7250d4eb6',1,'Client::sendRequestInfo()'],['../class_client_logic_engine.html#a14f91d94c71130f54df640d7df5acdcf',1,'ClientLogicEngine::sendRequestInfo()']]],
  ['server',['Server',['../class_server.html#aaf98d5194faee831c6340cc736b9b879',1,'Server']]],
  ['setupui',['setupUi',['../class_ui___client.html#adb27950aab943ec40e650cb7c8d9d196',1,'Ui_Client::setupUi()'],['../class_ui___main_window.html#acf4a0872c4c77d8f43a2ec66ed849b58',1,'Ui_MainWindow::setupUi()'],['../class_ui___start_form.html#abc5d5778aeef0c8ab474dd233b0a613f',1,'Ui_StartForm::setupUi()']]],
  ['startform',['StartForm',['../class_start_form.html#a7b0f80a26f2cbe708c23e879c10f744c',1,'StartForm']]],
  ['stopsearch',['stopSearch',['../class_client.html#af2dd47e936d2f073041c14a8e3444465',1,'Client::stopSearch()'],['../class_client_logic_engine.html#af1cfd4a5d7e02409302079d1a7975093',1,'ClientLogicEngine::stopSearch()']]],
  ['sum',['sum',['../class_client_logic_engine.html#a2d8a2313cc6e81340888656970a5ce19',1,'ClientLogicEngine::sum()'],['../class_client_handler_socket.html#a2b6a54abfa8d2562508c809bb555aa2f',1,'ClientHandlerSocket::sum()']]]
];
