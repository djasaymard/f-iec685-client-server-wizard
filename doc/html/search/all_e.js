var searchData=
[
  ['sendrequest',['sendRequest',['../class_client_logic_engine.html#ad30f486e7449eb99fa9289115dbfbff0',1,'ClientLogicEngine']]],
  ['sendrequestcommand',['sendRequestCommand',['../class_client.html#af73f84d5c2a86451edc33e29903fa007',1,'Client::sendRequestCommand()'],['../class_client_logic_engine.html#ace1e702a41758be0308474b8c1e4b214',1,'ClientLogicEngine::sendRequestCommand()']]],
  ['sendrequestinfo',['sendRequestInfo',['../class_client.html#a9880e1095f64455f0d9e33d7250d4eb6',1,'Client::sendRequestInfo()'],['../class_client_logic_engine.html#a14f91d94c71130f54df640d7df5acdcf',1,'ClientLogicEngine::sendRequestInfo()']]],
  ['server',['Server',['../class_server.html',1,'Server'],['../class_start_form.html#ae32f0f774ba7701a921e7169ee864f81',1,'StartForm::server()'],['../class_server.html#aaf98d5194faee831c6340cc736b9b879',1,'Server::Server()']]],
  ['server_2ecpp',['server.cpp',['../server_8cpp.html',1,'']]],
  ['server_2eh',['server.h',['../server_8h.html',1,'']]],
  ['serverstate',['serverState',['../class_start_form.html#a232a3aa690ff4c627e277dacd5702582',1,'StartForm']]],
  ['socket',['socket',['../class_client_logic_engine.html#a2d6b19b5adeb0bf58d3e9a65db8c8e4c',1,'ClientLogicEngine::socket()'],['../class_client_handler_socket.html#a76c94ff177d79ddede2e3f2fb4db1d6a',1,'ClientHandlerSocket::socket()']]],
  ['startform',['StartForm',['../class_start_form.html',1,'StartForm'],['../class_start_form.html#a7b0f80a26f2cbe708c23e879c10f744c',1,'StartForm::StartForm()']]],
  ['startform_2ecpp',['startform.cpp',['../startform_8cpp.html',1,'']]],
  ['startform_2eh',['startform.h',['../startform_8h.html',1,'']]],
  ['stopsearch',['stopSearch',['../class_client.html#af2dd47e936d2f073041c14a8e3444465',1,'Client::stopSearch()'],['../class_client_logic_engine.html#af1cfd4a5d7e02409302079d1a7975093',1,'ClientLogicEngine::stopSearch()']]],
  ['sum',['sum',['../class_client_logic_engine.html#a2d8a2313cc6e81340888656970a5ce19',1,'ClientLogicEngine']]]
];
